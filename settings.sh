APP="debian-desktop"

DESKTOP='lxde'
#DESKTOP='mate'
#DESKTOP='xfce'
#DESKTOP='kde'
#DESKTOP='cinnamon'

### Docker settings.
IMAGE="dockerscripts/debian-desktop:$DESKTOP"

### Forwarded ports
#X2GO_PORT="2202"
#PORTS="$X2GO_PORT:22"

### Epoptes admins. Uncomment to enable.
#EPOPTES_USERS="user1 user2"

### Admin account. Uncomment to enable.
#ADMIN_USER="admin"
#ADMIN_PASS="pass"

### Guest account. Uncomment to enable.
### This account will be used as a template for guest accounts.
#GUEST_USER="guest"
#GUEST_PASS="pass"
