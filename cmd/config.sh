cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    check_settings

    ds inject msmtp.sh
    ds inject logwatch.sh $(hostname)

    # custom config for each desktop
    ds inject desktop-config.sh
    
    # create and setup user accounts
    ds inject setup-user-accounts.sh
    [[ -f accounts.txt ]] || cp $APPDIR/misc/accounts.txt .
	ds users create accounts.txt

    # add an admin user
    [[ -n $ADMIN_USER ]] && \
        ds inject add-admin.sh $ADMIN_USER $ADMIN_PASS

    # add users to the group 'epoptes'
    for user in $EPOPTES_USERS; do
        ds exec adduser $user epoptes
    done
}

fail() { echo "$@" >&2; exit 1; }

check_settings() {
    [[ $ADMIN_PASS == 'pass' ]] \
	&& fail "Error: ADMIN_PASS on 'settings.sh' has to be changed for security reasons."

    [[ $GUEST_PASS == 'pass' ]] \
	&& fail "Error: GUEST_PASS on 'settings.sh' has to be changed for security reasons."
}
