#!/bin/bash -x

# get the latest version of debian image
docker pull debian:bookworm

# init an app
ds pull debian-desktop
rm -rf /var/ds/build-dd-images/
ds init debian-desktop @build-dd-images
cd /var/ds/build-dd-images/

sed -i settings.sh \
    -e '/#DESKTOP=/d'

for desktop in lxde xfce mate kde cinnamon ; do
    sed -i settings.sh \
	-e "/DESKTOP=/ c DESKTOP='$desktop'"
    ds build
    docker push dockerscripts/debian-desktop:$desktop
done

# clean up
cd ..
rm -rf /var/ds/build-dd-images/
docker container prune --force
docker image prune --force
