include(bookworm)

include(dockerfile/xrdp)
include(dockerfile/misc)

define(DESKTOP,`esyscmd(`printf ${DESKTOP:-lxde}')')dnl
include(dockerfile/desktop-DESKTOP)

### install additional packages
sinclude(packages)
