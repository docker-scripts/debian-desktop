#!/bin/bash -x

source /host/settings.sh

config-cinnamon() {
    # suppress the warning 'Running in software rendering mode' of cinnamon
    echo 'export CINNAMON_2D=true' > /etc/X11/Xsession.d/99cinnamon2d

    # script for session logout
    cat <<EOF > /usr/local/bin/session-logout.sh
#!/bin/bash
cinnamon-session-quit --no-prompt --force --logout
EOF
    chmod +x /usr/local/bin/session-logout.sh
}

config-mate() {
    # script for session logout
    cat <<EOF > /usr/local/bin/session-logout.sh
#!/bin/bash
mate-session-save --force-logout
EOF
    chmod +x /usr/local/bin/session-logout.sh
}

config-xfce() {
    # script for session logout
    cat <<EOF > /usr/local/bin/session-logout.sh
#!/bin/bash
xfce4-session-logout --logout
EOF
    chmod +x /usr/local/bin/session-logout.sh

    sed -i /etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml \
	-e 's%<property name="ThemeName" type="string" value="Xfce"/>%<property name="ThemeName" type="string" value="Raleigh"/>%' 

    # disable xfwm4 compositing if X extension COMPOSITE is missing and no config file exists
    local configfile="~/.config/xfce4/xfconf/xfce-perchannel-xml/xfwm4.xml"
    cat <<EOF > /usr/local/bin/start
#!/bin/bash
xdpyinfo | grep -q -i COMPOSITE || {
  echo 'x11docker/xfce: X extension COMPOSITE is missing.
Window manager compositing will not work.
If you run x11docker with option --nxagent,
you might want to add option --composite.' >&2
  [ -e $configfile ] || {
    mkdir -p $(dirname $configfile)
    echo '<?xml version="1.0" encoding="UTF-8"?>
<channel name="xfwm4" version="1.0">

  <property name="general" type="empty">
    <property name="use_compositing" type="bool" value="false"/>
  </property>
</channel>
' > $configfile
  }
}
startxfce4
EOF
    chmod +x /usr/local/bin/start 
}

config-lxde() {
    # script for session logout
    cat <<EOF > /usr/local/bin/session-logout.sh
#!/bin/bash
pkill -KILL -u \$USER
EOF
    chmod +x /usr/local/bin/session-logout.sh

    # GTK 2 and 3 settings for icons and style, wallpaper
    cat <<EOF > /etc/skel/.gtkrc-2.0
gtk-theme-name="Raleigh"
gtk-icon-theme-name="nuoveXT2"
EOF

    mkdir -p /etc/skel/.config/gtk-3.0
    cat <<EOF > /etc/skel/.config/gtk-3.0/settings.ini
[Settings]
gtk-theme-name="Raleigh"
gtk-icon-theme-name="nuoveXT2"
EOF

    mkdir -p /etc/skel/.config/pcmanfm/LXDE
    cat <<EOF > /etc/skel/.config/pcmanfm/LXDE/desktop-items-0.conf
[*]
wallpaper_mode=stretch
wallpaper_common=1
wallpaper=/usr/share/lxde/wallpapers/lxde_blue.jpg
EOF

    mkdir -p /etc/skel/.config/libfm
    cat <<EOF > /etc/skel/.config/libfm/libfm.conf
[config]
quick_exec=1
terminal=lxterminal
EOF

    mkdir -p /etc/skel/.config/openbox/
    cat <<EOF > /etc/skel/.config/openbox/lxde-rc.xml
<?xml version="1.0" encoding="UTF-8"?>
<theme>
  <name>Clearlooks</name>
</theme>
EOF

    mkdir -p /etc/skel/.config/
    cat <<EOF > /etc/skel/.config/mimeapps.list
[Added Associations]
text/plain=mousepad.desktop;
EOF
}

config-kde() {
    # script for session logout
    cat <<EOF > /usr/local/bin/session-logout.sh
#!/bin/bash
qdbus org.kde.ksmserver /KSMServer logout 0 0 0
EOF
    chmod +x /usr/local/bin/session-logout.sh
}

# call the correct config function
config-$DESKTOP
