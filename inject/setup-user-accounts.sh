#!/bin/bash -x
set -e
source /host/settings.sh

# main function
main() {
    # setup config
    setup_configuration_of_new_accounts
    setup_screensaver
    setup_autologout_when_idle
    setup_guest_accounts

    # enable password authentication
    sed -i /etc/ssh/sshd_config \
        -e "/PasswordAuthentication no\$/ c PasswordAuthentication yes"
    systemctl restart ssh
}

setup_configuration_of_new_accounts() {
    # create a group for student accounts
    [[ -z $(getent group student) ]] && addgroup student --gid=990

    # make new accounts belong to the student group
    sed -i /etc/adduser.conf \
	-e '/^USERGROUPS=/ c USERGROUPS=no' \
	-e '/^USERS_GID=/ c USERS_GID=990' \
	-e '/^DIR_MODE=/ c DIR_MODE=0700'

    # customize .bashrc of new accounts
    cat <<'EOF' > /usr/local/sbin/adduser.local
#!/bin/bash
user_home=$4
sed -i $user_home/.bashrc -e '/^#force_color_prompt=/c force_color_prompt=yes'
EOF
    chmod +x /usr/local/sbin/adduser.local

    # place some resource limits
    sed -i /etc/security/limits.conf -e '/student/d'
    cat <<EOF >> /etc/security/limits.conf
### student
@student        hard    nproc           10000
@student        hard    cpu             2
@student        hard    maxlogins       3
EOF
}

setup_screensaver() {
    # autostart xscreensaver on login
    cat <<EOF > /etc/xdg/autostart/xscreensaver.desktop
[Desktop Entry]
Type=Application
Exec=xscreensaver
Hidden=false
Name[C]=xscreensaver
Name=xscreensaver
EOF
    # set default xscreensaver options
    cat <<EOF > /etc/skel/.xscreensaver
splash:         False
lock:           False
mode:           random
timeout:        0:05:00
cycle:          0:03:00
EOF
}

setup_autologout_when_idle() {
    cat <<EOF > /usr/local/bin/autologout.sh
#!/bin/bash
xautolock -time 10 -locker /usr/local/bin/session-logout.sh
EOF
    chmod +x /usr/local/bin/autologout.sh

    cat <<EOF > /etc/xdg/autostart/autologout.desktop
[Desktop Entry]
Type=Application
Exec=/usr/local/bin/autologout.sh
Hidden=false
Name[C]=autologout
Name=autologout
EOF
}

setup_guest_accounts() {
    [[ -n $GUEST_USER ]] || return
    [[ -n $GUEST_PASS ]] || return

    # create the template/skeleton guest account
    groupadd guest --gid=500 -f
    adduser $GUEST_USER --uid=500 --gid=500 \
            --shell=/bin/bash --gecos '' \
            --disabled-password
    usermod $GUEST_USER --password="$(openssl passwd -stdin <<< $GUEST_PASS)"
    chown $GUEST_USER:guest -R /home/$GUEST_USER
    chmod +rx -R /home/$GUEST_USER

    # increase the timeout of the screensaver
    sed -i /home/$GUEST_USER/.xscreensaver \
	-e '/^timeout:/ c timeout:  0:20:00'

    # create a script to dump the configuration of GUEST_USER
    # it should be called only by the GUEST_USER
    cat <<EOF > /usr/local/bin/save-guest-config.sh
#!/bin/bash -x
[[ \$(whoami) == '$GUEST_USER' ]] || exit 1
chmod +rx -R .
dconf dump / > .dconf
EOF
    chmod +x /usr/local/bin/save-guest-config.sh

    # create a script that resets a guest account
    cat <<EOF > /usr/local/bin/reset-guest-account.sh
#!/bin/bash
[[ \$HOME =~ ^/home/guest-accounts/.*\$ ]] || exit 1
cd \$HOME || exit 2
rm -rf .* *
rsync -a /home/$GUEST_USER/ .
chown \$USER -R .
chmod go-rwx -R .
if [[ -f .dconf ]]; then
    dconf load / < .dconf
    rm .dconf
fi
EOF
    chmod +x /usr/local/bin/reset-guest-account.sh

    # make sure guest accounts are reset on login
    cat <<EOF > /etc/profile.d/reset-guest-account.sh
#!/bin/bash
/usr/local/bin/reset-guest-account.sh
EOF

    ### place some limits on guest accounts
    sed -i /etc/security/limits.conf -e '/guest/d'
    cat <<EOF >> /etc/security/limits.conf
### guest
@guest        hard    nproc           1000
@guest        hard    cpu             2
@guest        hard    maxlogins       1
EOF
}

# call the main function
main
